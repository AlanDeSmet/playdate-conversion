# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


#BRAILLE = "⠀⠁⠈⠉⠂⠃⠊⠋⠐⠑⠘⠙⠒⠓⠚⠛⠄⠅⠌⠍⠆⠇⠎⠏⠔⠕⠜⠝⠖⠗⠞⠟⠠⠡⠨⠩⠢⠣⠪⠫⠰⠱⠸⠹⠲⠳⠺⠻⠤⠥⠬⠭⠦⠧⠮⠯⠴⠵⠼⠽⠶⠷⠾⠿⡀⡁⡈⡉⡂⡃⡊⡋⡐⡑⡘⡙⡒⡓⡚⡛⡄⡅⡌⡍⡆⡇⡎⡏⡔⡕⡜⡝⡖⡗⡞⡟⡠⡡⡨⡩⡢⡣⡪⡫⡰⡱⡸⡹⡲⡳⡺⡻⡤⡥⡬⡭⡦⡧⡮⡯⡴⡵⡼⡽⡶⡷⡾⡿⢀⢁⢈⢉⢂⢃⢊⢋⢐⢑⢘⢙⢒⢓⢚⢛⢄⢅⢌⢍⢆⢇⢎⢏⢔⢕⢜⢝⢖⢗⢞⢟⢠⢡⢨⢩⢢⢣⢪⢫⢰⢱⢸⢹⢲⢳⢺⢻⢤⢥⢬⢭⢦⢧⢮⢯⢴⢵⢼⢽⢶⢷⢾⢿⣀⣁⣈⣉⣂⣃⣊⣋⣐⣑⣘⣙⣒⣓⣚⣛⣄⣅⣌⣍⣆⣇⣎⣏⣔⣕⣜⣝⣖⣗⣞⣟⣠⣡⣨⣩⣢⣣⣪⣫⣰⣱⣸⣹⣲⣳⣺⣻⣤⣥⣬⣭⣦⣧⣮⣯⣴⣵⣼⣽⣶⣷⣾⣿"
#print(len(BRAILLE))


BG_BLACK="\u001b[40m"
FG_WHITE="\u001b[37m"
WHITE_ON_BLACK=BG_BLACK+FG_WHITE
RESET="\u001b[0m"

class BinaryImage:
    def __init__(self, characters, width, height):
        self.characters = characters
        self.width = width
        self.height = height
        num_chars = len(self.characters)
        needed_chars = 2**(self.width*self.height)
        assert num_chars == needed_chars, f"num_chars != needed_chars   {num_chars} != {needed_chars}"

    def get_single_cell(self, pixels, row, col):
        img_height = len(pixels)
        img_width = len(pixels[0])
        idx = 0
        nextval = 1
        for subrow in range(row, min(img_height, row+self.width)):
            for subcol in range(col, min(img_width, col+self.height)):
                if pixels[subrow][subcol]:
                    idx += nextval
                nextval *= 2
        return self.characters[idx]

    def full_image(self, outstream, pixels):
        img_height = len(pixels)
        img_width = 0 if img_height == 0 else len(pixels[0])
        for row in range(0, img_height, self.height):
            outrow = ""
            for col in range(0, img_width, self.width):
                # Fixes broken U+2595 	▕ 	Right one eighth block  on Konsole
                # it bleeds into the right character if it's a normal space.
                outrow = outrow.replace( "▐ ", "▐\u00A0")
                outrow += self.get_single_cell(pixels, row, col)
            outstream.write(WHITE_ON_BLACK+
                    outrow+"\n"+
                    RESET
                    )


patterns2by4 = \
    "⠀⠁⠈⠉⠂⠃⠊⠋⠐⠑⠘⠙⠒⠓⠚⠛"+\
    "⠄⠅⠌⠍⠆⠇⠎⠏⠔⠕⠜⠝⠖⠗⠞⠟"+\
    "⠠⠡⠨⠩⠢⠣⠪⠫⠰⠱⠸⠹⠲⠳⠺⠻"+\
    "⠤⠥⠬⠭⠦⠧⠮⠯⠴⠵⠼⠽⠶⠷⠾⠿"+\
    "⡀⡁⡈⡉⡂⡃⡊⡋⡐⡑⡘⡙⡒⡓⡚⡛"+\
    "⡄⡅⡌⡍⡆⡇⡎⡏⡔⡕⡜⡝⡖⡗⡞⡟"+\
    "⡠⡡⡨⡩⡢⡣⡪⡫⡰⡱⡸⡹⡲⡳⡺⡻"+\
    "⡤⡥⡬⡭⡦⡧⡮⡯⡴⡵⡼⡽⡶⡷⡾⡿"+\
    "⢀⢁⢈⢉⢂⢃⢊⢋⢐⢑⢘⢙⢒⢓⢚⢛"+\
    "⢄⢅⢌⢍⢆⢇⢎⢏⢔⢕⢜⢝⢖⢗⢞⢟"+\
    "⢠⢡⢨⢩⢢⢣⢪⢫⢰⢱⢸⢹⢲⢳⢺⢻"+\
    "⢤⢥⢬⢭⢦⢧⢮⢯⢴⢵⢼⢽⢶⢷⢾⢿"+\
    "⣀⣁⣈⣉⣂⣃⣊⣋⣐⣑⣘⣙⣒⣓⣚⣛"+\
    "⣄⣅⣌⣍⣆⣇⣎⣏⣔⣕⣜⣝⣖⣗⣞⣟"+\
    "⣠⣡⣨⣩⣢⣣⣪⣫⣰⣱⣸⣹⣲⣳⣺⣻"+\
    "⣤⣥⣬⣭⣦⣧⣮⣯⣴⣵⣼⣽⣶⣷⣾⣿"
binary2by4 = BinaryImage(patterns2by4, 2, 4)

patterns2by3 = \
    "⠀⠁⠈⠉⠂⠃⠊⠋⠐⠑⠘⠙⠒⠓⠚⠛"+\
    "⠄⠅⠌⠍⠆⠇⠎⠏⠔⠕⠜⠝⠖⠗⠞⠟"+\
    "⠠⠡⠨⠩⠢⠣⠪⠫⠰⠱⠸⠹⠲⠳⠺⠻"+\
    "⠤⠥⠬⠭⠦⠧⠮⠯⠴⠵⠼⠽⠶⠷⠾⠿"
binary2by3 = BinaryImage(patterns2by3, 2, 3)

binary1by2 = BinaryImage(" ▀▄█", 2, 1)

patterns2by2 = \
    " ▘▝▀"+\
    "▖▌▞▛"+\
    "▗▚▐▜"+\
    "▄▙▟█" 
binary2by2 = BinaryImage(patterns2by2, 2, 2)

TOP="▀"
class AlphaBinaryImage:
    def full_image(self, outstream, pixels, alpha):
        img_height = len(pixels)
        img_width = len(pixels[0])
        prev = (None,None)
        for row in range(0, img_height, 2):
            prev = (None,None)
            outrow = ""
            for col in range(0, img_width):
                if alpha[row][col]:
                    if pixels[row][col]: fg = 37
                    else:                fg = 30
                else:
                    if pixels[row][col]: fg = 35
                    else:                fg = 34

                if (row+1) < img_height:
                    if alpha[row+1][col]:
                        if pixels[row+1][col]: bg = 47
                        else:                  bg = 40
                    else:
                        if pixels[row+1][col]: bg = 45
                        else:                  bg = 44
                else:
                    bg = 40
                this = (fg,bg)
                if this != prev:
                    outrow += f"\u001b[{fg}m\u001b[{bg}m"
                    prev = this
                outrow += TOP
            outstream.write(outrow+RESET+"\n")
