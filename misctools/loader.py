# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import itertools

loaders = []

class LoadedResults:
    def __init__(self, images=None, fps=None):
        self.images = images or []
        self.fps = fps


class Loader:
    def __init__(self, signature = None):
        self.signature = signature

    def register(self):
        loaders.append(self)

    def can_load(self, signature):
        assert self.signature != None, "Loader.signature must be set or Loader.can_load overridden"
        return signature == self.signature

    def load_post_signature(self, file):
        raise NotImplementedError("Loader.load must be overridden")


def find_loader(file):
    signature = file.read(12)
    #print("signature:",signature)
    for loader in loaders:
        if loader.can_load(signature):
            return loader
    return None





################################################################################

import playdatefiles.load.pdi

class PDILoader(Loader):
    def __init__(self):
        super().__init__(playdatefiles.load.pdi.signature)

    def load_post_signature(self, file):
        img = playdatefiles.load.pdi.load_post_signature(file)
        return LoadedResults(images=[img])

PDILoader().register()



################################################################################

import playdatefiles.load.pdt 

class PDTLoader(Loader):
    def __init__(self):
        super().__init__(playdatefiles.load.pdt.signature)

    def load_post_signature(self, file):
        imgs = playdatefiles.load.pdt.load_post_signature(file)
        imgs = list(itertools.chain(*imgs))
        return LoadedResults(images=imgs)

PDTLoader().register()

################################################################################

import playdatefiles.load.pdv 

class PDVLoader(Loader):
    def __init__(self):
        super().__init__(playdatefiles.load.pdv.signature)

    def load_post_signature(self, file):
        images,fps = playdatefiles.load.pdv.load_post_signature(file)
        return LoadedResults(images=images, fps=fps)

PDVLoader().register()


