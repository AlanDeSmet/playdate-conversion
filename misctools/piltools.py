# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import struct
import io
import itertools

import PIL.Image

def flatten_2d(arr):
    return list(itertools.chain(*arr))

def image_width(img):
    return len(img.pixels[0])
def image_height(img):
    return len(img.pixels)

def pil_image_from_flat_list(list, size):
    img = PIL.Image.new("L", size)
    img.putdata(list, scale=255)
    return img

def pil_image_from_image(img):
    size = (image_width(img), image_height(img))
    pimg = pil_image_from_flat_list(flatten_2d(img.pixels), size)
    if img.alpha is not None:
        palpha = pil_image_from_flat_list(flatten_2d(img.alpha), size)
        pimg.putalpha(palpha)
    return pimg

