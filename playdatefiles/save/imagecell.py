# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import math

def unpack_bits(bytes):
    """ return a list where each entry is a single bit from the provided bytes """
    result = []
    for byte in bytes:
        result += [
                (byte & byte_masks[0]) != 0,
                (byte & byte_masks[1]) != 0,
                (byte & byte_masks[2]) != 0,
                (byte & byte_masks[3]) != 0,
                (byte & byte_masks[4]) != 0,
                (byte & byte_masks[5]) != 0,
                (byte & byte_masks[6]) != 0,
                (byte & byte_masks[7]) != 0,
                ]
    return result

def pack_2d(data):
    result = b''
    rowlen = len(data[0])
    for row in data:
        assert len(row) == rowlen
        for i in range(0, rowlen, 8):
            byte = row[i:i+8]
            needed = 8 - len(byte)
            assert needed >= 0
            byte += [0]*needed
            packed_byte = 0
            for i in range(8):
                bit = 1 if byte[i] else 0
                packed_byte = bit + (packed_byte<<1)
                #print(packed_byte, bit)
            result += bytes([packed_byte])
    #print(result)
    return result

def image_width(img):
    return len(img.pixels[0])
def image_height(img):
    return len(img.pixels)

def save_image_cell(file, image):
    width = image_width(image)
    height = image_height(image)
    stride = math.ceil(width/8)
    (clip_left, clip_right, clip_top, clip_bottom) = (0,0,0,0)
    flags = 0
    if image.alpha is not None:
        flags |= 0x3
    # TODO: ^ is 0x4 set here sometimes? Always?
    file.write(
            struct.pack('<7Hh', width, height, stride, 
                clip_left, clip_right, clip_top, clip_bottom,
                flags))
    img = pack_2d(image.pixels)
    assert len(img) == stride*height
    file.write(img)
    if image.alpha is not None:
        img = pack_2d(image.alpha)
        assert len(img) == stride*height
        file.write(img)
