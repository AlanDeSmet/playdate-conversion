#! /usr/bin/python3

# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import zlib
import io
from playdatefiles.save.imagecell import save_image_cell

signature = b"Playdate IMG"

extension = "pdi"

def save(file, image):
    file.write(signature)
    file.write(struct.pack('>I', 0)) # Flags. 0x1<<7 for compressed
    save_image_cell(file, image)


