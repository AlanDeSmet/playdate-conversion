#! /usr/bin/python3

# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import zlib
import io
from playdatefiles.load.imagecell import load_image_cell, CompressedImagesHeader

signature = b"Playdate IMT"

extension="pdt"


def load(file):
    foundsig = file.read(12)
    if foundsig != signature:
        raise ValueError(f"{file} is not a valid Playdate {extension} file; it should begin with {signature} but begins with {foundsig}")
    return load_post_signature(file)

def load_post_signature(file):
    flags = struct.unpack('>I', file.read(4))[0]
    compressed = bool((flags>>7)&0x1)
    #print("     Compressed?", compressed)

    if compressed:
        # We don't really need any of this information, but it's here.
        unused = CompressedImagesHeader(file.read(16))
        
        file = io.BytesIO(zlib.decompress(file.read()))

    total_images, images_per_row = struct.unpack("<HH", file.read(4))
    #print(f"{total_images} cells total, {images_per_row} per row, {total_images//images_per_row} per column")

    offsets = [0]
    for idx in range(total_images):
        bytes = file.read(4)
        offset = struct.unpack('<i', bytes)[0]
        offsets.append(offset)
        #print(f"{idx}. offset {offsets[-1]}")

    cellstart = file.tell()

    images = []
    for col in range(images_per_row):
        images.append([None]*(total_images//images_per_row))
    row = []
    for i in range(total_images):
        goal = cellstart + offsets[i]
        row = i // images_per_row
        col = i - row*images_per_row
        #print(f"{i+1}/{total_images}, @{file.tell()} vs {goal}, -> {col},{row}")
        file.seek(goal)
        img = load_image_cell(file)
        images[col][row] = img
    #print(f"Images is {len(images[0])} wide and {len(images)} tall")
    return images



