#! /usr/bin/python3

# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import zlib
import io
from collections import namedtuple

from playdatefiles.load.imagetools import unpack_bits_2d

signature = b"Playdate VID"

extension = "pdv"


def xor_2d_bit_arrays(a,b):
    """ Return result of exclusive or'ing each element a and b """
    result = []
    for rowa, rowb in zip(a,b):
        newrow = [cella^cellb for cella,cellb in zip(rowa, rowb)]
        result.append(newrow)
    return result

def load(file):
    foundsig = file.read(12)
    if foundsig != signature:
        raise ValueError(f"{file} is not a valid Playdate {extension} file; it should begin with {signature} but begins with {foundsig}")
    return load_post_signature(file)

FrameHeader = namedtuple('FrameHeader', ['offset', 'type'])
Image = namedtuple('Image', ['pixels', 'alpha'])

def load_post_signature(file):
    unknown = file.read(4)
    if unknown != b'\x00\x00\x00\x00':
        print(f"PDV unknowns are {repr(unknown)}")

    num_frames, unknown, fps, width, height = struct.unpack('<hhfhh', file.read(12))
    #print(f"{num_frames} frames at {fps} f/s  {width}×{height}")

    frameinfo = []
    for frame in range(num_frames+1):
        mix =  struct.unpack('<I', file.read(4))[0]
        offset = mix >> 2
        type = mix & 0x3
        frameinfo.append(FrameHeader(offset, type))
        VALID_FIRST_FRAME_TYPES = [1,3]
        if frame == 0 and type not in VALID_FIRST_FRAME_TYPES:
            raise ValueError(f"First frame must be in {VALID_FIRST_FRAME_TYPES}, but it's {type}")
        if frame < num_frames and type == 0:
            raise ValueError(f"frame {frame+1} out of {num_frames} is of type 0, but that is only allowed for the false frame at the end")

    #print(", ".join([hex(a.offset) for a in frameinfo]))

    #print(f"at start of frames {hex(file.tell())}")
    images = []
    last_bits = None
    for frame in range(num_frames):
        fthis = frameinfo[frame]
        #print(f"Frame {frame:4d}; offset {hex(fthis.offset):6s} ; type {fthis.type} ")
        fnext = frameinfo[frame+1]
        needed = fnext.offset - fthis.offset
        compressed = file.read(needed)
        raw = zlib.decompress(compressed)
        stride = width//8
        if width%8 > 0: stride += 1
        bits = unpack_bits_2d(raw, stride, height)
        if fthis.type == 1: # I-Frame
            pass # No processing needed, this is complete
        elif fthis.type == 2: # P-Frame
            bits = xor_2d_bit_arrays(bits, last_bits)
        else:
            raise Exception(f"Internal error: encountered frame type {fthis.type} in frame {frame}, but that should have been cause earlier")
        last_bits = bits
        img = Image(bits,None)
        images.append(img)
    return images, fps

