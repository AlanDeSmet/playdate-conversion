# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import itertools


byte_masks = list([1<<x for x in range(7,-1,-1)])

def unpack_bits(bytes):
    """ return a list where each entry is a single bit from the provided bytes """
    result = []
    for byte in bytes:
        result += [
                (byte & byte_masks[0]) != 0,
                (byte & byte_masks[1]) != 0,
                (byte & byte_masks[2]) != 0,
                (byte & byte_masks[3]) != 0,
                (byte & byte_masks[4]) != 0,
                (byte & byte_masks[5]) != 0,
                (byte & byte_masks[6]) != 0,
                (byte & byte_masks[7]) != 0,
                ]
    return result

def unpack_bits_2d(indata, stride, height):
    """ Return list of lists for 2d packed bits 

    stride is width of the array in bytes.

    len(result) will equal height

    len(result[0]) will equal stride*8
    """
    assert len(indata) == stride*height
    i = 0
    data = []
    for row in range(height):
        packed_image = indata[i:i+stride]
        image = unpack_bits(packed_image)
        data.append(image)
        i+=stride
    return data

