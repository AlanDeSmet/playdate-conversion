#! /usr/bin/python3

# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import zlib
import io
from playdatefiles.load.imagecell import load_image_cell, CompressedImagesHeader

signature = b"Playdate IMG"

extension = "pdi"

def load(file):
    foundsig = file.read(12)
    if foundsig != signature:
        raise ValueError(f"{file} is not a valid Playdate {extension} file; it should begin with {signature} but begins with {foundsig}")
    return load_post_signature(file)

def load_post_signature(file):
    flags = struct.unpack('>I', file.read(4))[0]
    compressed = bool((flags>>7)&0x1)
    #print("     Compressed?", compressed)
    
    if compressed:
        # We don't really need any of this information, but it's here.
        # Note that for pdi, the last field, num_cells, is unused.
        unused = CompressedImagesHeader(file.read(16))

        file = io.BytesIO(zlib.decompress(file.read()))

    return load_image_cell(file)


