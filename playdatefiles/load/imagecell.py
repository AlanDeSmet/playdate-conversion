# This file is part of the Alan's Playdate Conversion Tools.
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import struct
import itertools
from collections import namedtuple

from playdatefiles.load.imagetools import unpack_bits_2d

Image = namedtuple('Image', ['pixels', 'alpha'])

class CompressedImagesHeader:
    """ Stores the extra header when a pdi/pdt file is compressed

    In Python it's of little value.

    decompressed_size might be useful in a language where you need to
    pre-allocate space for it.

    width and height are redundant with the information in the cell headers,
    although it may be more convenient since you don't need to account
    for the clipping values.

    num_images is unused for pdi and strictly redundant with edata
    immediately following in pdt.
    """
    def __init__(self, bytes_16):
        assert len(bytes_16) == 16, f"compressed image header must be 16 bytes long, but it's {len(bytes_16)}"
        (
            self.decompressed_size,
            self.image_width,
            self.image_height,
            self.num_cells) = \
                struct.unpack('<iiii', bytes_16)

class CellHeader:
    def __init__(self, bytes_16):
        assert len(bytes_16) == 16, f"cell header must be 16 bytes long, but it's {len(bytes_16)}"

        (self.width, self.height, self.stride,
            self.clip_left, self.clip_right, self.clip_top, self.clip_bottom)=\
            struct.unpack('<7H', bytes_16[:14])
        self.flags = struct.unpack('<h', bytes_16[14:16])[0]
        #print("cell:")
        #print(f"  pixels: {self.width}×{self.height}, stride {self.stride}")
        #print(f"  clip L {self.clip_left}, R {self.clip_right}, T {self.clip_top}, B {self.clip_bottom}")
        #print(f"  flags", format(self.flags,"016b"))
        self.hasalpha = bool(self.flags&0x3)
        #print("   has alpha data", self.hasalpha)

    #def bytes_to_read(self):
    #    return self.stride*self.height
    
    def was_clipped(self):
        return max(self.clip_left, self.clip_right, self.clip_top, self.clip_bottom) > 0

    def real_width(self):
        return self.clip_left + self.clip_right + self.width

    def real_height(self):
        return self.clip_top + self.clip_bottom + self.height

def unpack_cell_pixels(file, cellheader):
    onerow =    [False]*cellheader.real_width()
    padleft =   [False] * cellheader.clip_left
    padright =  [False] * cellheader.clip_right

    packed_data = file.read(cellheader.stride*cellheader.height)
    corepixels = unpack_bits_2d(packed_data, cellheader.stride, cellheader.height)

    # []+ is to avoid aliasing
    data = []+[onerow]*cellheader.clip_top
    for row in corepixels:
        row = row[:cellheader.width]
        data.append(padleft+row+padright)
    data += [onerow]*cellheader.clip_bottom
    return data

def simple_alpha(cellheader):
    onerow = [False]*cellheader.real_width()
    data = []
    for i in range(cellheader.clip_top):
        data.append(onerow)
    for i in range(cellheader.height):
        row = (
            [False]*cellheader.clip_left +
            [True]*cellheader.width +
            [False]*cellheader.clip_right
                )
        data.append(row)
    for i in range(cellheader.clip_bottom):
        data.append(onerow)
    return data

def load_image_cell(file):
    cellheader = CellHeader(file.read(16))

    data = unpack_cell_pixels(file, cellheader)
    alpha = None
    if cellheader.hasalpha:
        alpha = unpack_cell_pixels(file, cellheader)
    elif (not cellheader.hasalpha) and cellheader.was_clipped():
        alpha = simple_alpha(cellheader)
    return Image(data, alpha)

