Alan's Playdate Conversion Tools
================================

These are tools for converting to and from various file formats used by Panic's
Playdate game system.

For information on the formats, see https://github.com/jaames/playdate-reverse-engineering

This project isn't affiliated with or endorsed by Panic in any way.

**The API is not finalized!**


API
===

The core library is in `playdatefiles`, one submodule for loading and one for
saving. There is one subsubmodule per Playdate file type. So you might `import
playdatefiles.load.pdi` to read Playdate PDI files.

The `playdatefiles` submodules are designed to be largely independent. You can
delete `playdatefiles.load` or `playdatefiles.save` if you don't need them.
You can delete `playdatefiles.{load,save}.pd?.py` if you don't need support
for a given file type.

Each `pd?.py` subsubmodule has `signature`, a variable containing the 12-byte
signature that starts that Playdate file type. For example, for PDI files, it's
`b"Playdate IMG"`. It also has `extension`, the typical file extention for that
type. For PDI files, it's `"pdi"`.

The `playdatefiles.load` modules have a `load` function while the
`playdatefiles.save` modules have a `save` function. For image and video file
formats, they take or return one or more image objects.

An image object must have `.pixels` and `.alpha`. `.alpha` may be `None`,
indicating no alpha channel. Both are 2d (nested) arrays, one entry per pixel.
They are row major, so you'll address it like `some_image.pixels[row][col]` or
`some_image.pixels[y][x]`. (This is because the files for Playdate and the
Python Pillow module are stored in row-major order.)

Each row must be the same length.  Both must be the same size. The images width
is `len(some_image.pixels[0])`, while the height is `len(some_image.pixels)`.


Copyright
=========

Alan's Playdate Conversion Tools are copyright 2022 Alan De Smet.

Alan's Playdate Conversion Tools is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Alan's Playdate Conversion Tools is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License along with
Alan's Playdate Conversion Tools. If not, see <https://www.gnu.org/licenses/>.
