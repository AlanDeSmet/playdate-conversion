- Add minimal image writing library so we can be useful without PIL
- Should usefully handle multiple files
- Allow output file of stdout (probably "-")
- Create example assets in Python:
	- Eliminate dependencies on GNU Make and and Bourne shell and friends.
	- Ensure pixel perfect results for creating tests.
- At the moment, image pixel data is row major, but image tables are column major. Pick one. Maybe row major (matches file format), but offer API for get(x,y)?

Weird stuff:
- Season-001/Spellcorked.pdx/images/beauder.pdt has 0×0 images!
